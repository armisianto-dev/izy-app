import axios from 'axios';

export default axios.create({
    baseURL: 'http://api-residential-staging.izy.ai/v1/'
})