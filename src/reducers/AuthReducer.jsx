import AuthConstant from '../constants/AuthConstant';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case AuthConstant.AUTH_REQUEST:
            return {
                loggingIn: true,
                user: action.user
            };
        case AuthConstant.AUTH_SUCCESS:
            return {
                loggingIn: true,
                user: action.user
            };
        case AuthConstant.AUTH_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case AuthConstant.AUTH_CLEAR_ALERT:
            return {
                ...state,
                error: ''
            }
        case AuthConstant.AUTH_LOGOUT:
            return {};
        default:
            return state;
    }
};

export default AuthReducer;