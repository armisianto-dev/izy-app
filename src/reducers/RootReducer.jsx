import { combineReducers } from 'redux';
import AlertReducer from '../reducers/AlertReducer';
import AuthReducer from '../reducers/AuthReducer';
import MenuReducer from '../reducers/MenuReducer';

const RootReducer = combineReducers({
    AlertReducer,
    AuthReducer,
    MenuReducer
});

export default RootReducer;
