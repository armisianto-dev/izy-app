import AlertConstant from "../constants/AlertConstant";
const AlertReducer = (state = {}, action) => {
    switch (action.type) {
        case AlertConstant.SUCCESS:
            return {
                type: 'alert-success',
                message: action.message
            };
        case AlertConstant.ERROR:
            return {
                type: 'alert-error',
                message: action.message
            };
        case AlertConstant.CLEAR:
            return {}
        default:
            return state
    }
};

export default AlertReducer;