import MenuConstant from "../constants/MenuConstant";
const MenuReducer = (state = {}, action) => {
    switch (action.type) {
        // Get All
        case MenuConstant.GET_REQUEST:
            return {
                loading: true
            };
        case MenuConstant.GET_SUCCESS:
            return {
                items: action.menus.items,
                categories: action.menus.categories
            };
        case MenuConstant.GET_FAILURE:
            return {
                error: action.error
            };

        // Get Detail
        case MenuConstant.GET_DETAIL_REQUEST:
            return {
                ...state,
                loading: false
            };
        case MenuConstant.GET_DETAIL_SUCCESS:
            return {
                ...state,
                item: action.menu,
                showModal: true,
                form: action.form
            };
        case MenuConstant.GET_DETAIL_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case MenuConstant.GET_DETAIL_END:
            return {
                ...state,
                loading: false,
                showModal: false,
                form: ''
            }

        // Insert
        case MenuConstant.ADD_REQUEST:
            return {
                ...state,
                saving: true
            };
        case MenuConstant.ADD_SUCCESS:
            return {
                ...state,
                saving: false,
                saved: true,
                savingError: false,
                showModal: false,
                item: action.item
            };
        case MenuConstant.ADD_FAILURE:
            return {
                ...state,
                saving: false,
                savingError: true,
                showModal: true,
                error: action.error
            };

        // Update
        case MenuConstant.EDIT_REQUEST:
            return {
                ...state,
                saving: true
            };
        case MenuConstant.EDIT_SUCCESS:
            return {
                saving: false,
                saved: true,
                savingError: false,
                showModal: false,
                item: action.item
            };
        case MenuConstant.EDIT_FAILURE:
            return {
                ...state,
                saving: false,
                savingError: true,
                showModal: true,
                error: action.error
            };

        default:
            return state;
    }
}

export default MenuReducer;