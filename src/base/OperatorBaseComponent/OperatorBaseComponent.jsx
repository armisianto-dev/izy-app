import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import AuthAction from '../../actions/AuthAction';
import { PrivateRoute } from '../../component/PrivateRouteComponent/PrivateRouteComponent';
import TopBarComponent from '../../component/TopBarComponent/TopBarComponent';
import HomeComponent from '../../container/HomeComponent/HomeComponent';
import DetailMenuComponent from '../../container/MenuComponent/DetailMenuComponent/DetailMenuComponent';
import MenuComponent from '../../container/MenuComponent/MenuComponent';
import AuthCheck from '../../helpers/AuthCheck';

class OperatorBaseComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: AuthCheck
        }
    }

    render() {
        let { isLogin } = this.state;
        const { user, logout } = this.props;
        return (
            <Fragment>
                {isLogin && <TopBarComponent user={user} actionLogout={logout} />}
                <main role="main">
                    <PrivateRoute exact path="/" component={HomeComponent} />
                    <PrivateRoute exact path="/menu" component={MenuComponent} />
                    <PrivateRoute exact path="/menu/add">
                        <DetailMenuComponent action="add"></DetailMenuComponent>
                    </PrivateRoute>
                    <PrivateRoute exact path="/menu/:product" component={DetailMenuComponent} />
                </main>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    const { AuthReducer } = state;
    const { user } = AuthReducer;
    return { user };
}

const mapDispatchToProps = {
    logout: AuthAction.logout
}

export default connect(mapStateToProps, mapDispatchToProps)(OperatorBaseComponent);