import MenuConstant from "../constants/MenuConstant";
import MenuService from '../services/MenuService';

const getAll = () => {
    return dispatch => {
        dispatch(request());

        MenuService.getAll()
            .then(
                menus => {
                    dispatch(success(menus));
                },
                error => {
                    dispatch(failure('Menu not found'));
                }
        )
    }

    function request() {
        return {type: MenuConstant.GET_REQUEST}
    }

    function success(menus) {
        return {type: MenuConstant.GET_SUCCESS, menus}
    }

    function failure(error) {
        return {type: MenuConstant.GET_FAILURE, error}
    }
}

const getDetail = (menu, form) => {
    return dispatch => {
        dispatch(request());

        if (menu) {
            dispatch(success(menu, form));
        } else {
            dispatch(failure('Menu not found'));
        }
    }

    function request() {
        return { type: MenuConstant.GET_DETAIL_REQUEST }
    }

    function success(menu, form) {
        return { type: MenuConstant.GET_DETAIL_SUCCESS, menu, form }
    }

    function failure(error) {
        return { type: MenuConstant.GET_DETAIL_FAILURE, error }
    }
}

const closeDetail = () => {
    return dispatch => {
        dispatch({ type: MenuConstant.GET_DETAIL_END });
    }
}

const insertMenu = (params) => {
    return dispatch => {
        dispatch(request());

        MenuService.insertMenu(params)
            .then(
                item => {
                    dispatch(success(item));
                },
                error => {
                    dispatch(failure('Menu not found'));
                }
            )
    }

    function request() {
        return { type: MenuConstant.ADD_REQUEST }
    }

    function success(item) {
        return { type: MenuConstant.ADD_SUCCESS, item }
    }

    function failure(error) {
        return { type: MenuConstant.ADD_FAILURE, error }
    }
}


const updateMenu = (params) => {
    return dispatch => {
        dispatch(request());

        MenuService.updateMenu(params)
            .then(
                item => {
                    dispatch(success(item));
                },
                error => {
                    dispatch(failure('Menu not found'));
                }
            )
    }

    function request() {
        return { type: MenuConstant.EDIT_REQUEST }
    }

    function success(item) {
        return { type: MenuConstant.EDIT_SUCCESS, item }
    }

    function failure(error) {
        return { type: MenuConstant.EDIT_FAILURE, error }
    }
}

const MenuAction = {
    getAll,
    getDetail,
    closeDetail,
    insertMenu,
    updateMenu
}

export default MenuAction;