import AuthConstant from '../constants/AuthConstant';
import DefaultRedirect from '../helpers/DefaultRedirect';
import History from '../helpers/History';
import AuthService from '../services/AuthService';

const login = (email, password) => {
    return dispatch => {
        dispatch(request({ email }));

        AuthService.login(email, password)
        .then(
            user => {
                dispatch(success(user));
                DefaultRedirect('');
                History.push('/');
            },
            error => {
                dispatch(failure('Invalid username or password'));
            }
        )
    }

    function request(user) {
        return { type: AuthConstant.AUTH_REQUEST, user };
    }

    function success(user) {
        return { type: AuthConstant.AUTH_SUCCESS, user };
    }

    function failure(error) {
        return { type: AuthConstant.AUTH_FAILURE, error };
    }
}

const logout = () => {
    AuthService.logout();
    DefaultRedirect('');
    History.push('/');
    return {type: AuthConstant.AUTH_LOGOUT}
}

const clearAlert = () => {
    return { type: AuthConstant.AUTH_CLEAR_ALERT}
}

const AuthAction = {
    login,
    logout,
    clearAlert
}

export default AuthAction;