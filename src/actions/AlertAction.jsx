import AlertConstant from '../constants/AlertConstant';

const success = (message) => {
    return {
        type: AlertConstant.SUCCESS, message
    }
}

const error = (message) => {
    return {
        type: AlertConstant.ERROR, message
    }
}

const clear = () => {
    return {
        type: AlertConstant.CLEAR
    }
}

const AlertAction = {
    success,
    error,
    clear
}

export default AlertAction;