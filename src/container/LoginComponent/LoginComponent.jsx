import React from 'react';
import { connect } from 'react-redux';
import AuthAction from '../../actions/AuthAction';
import AuthCheck from '../../helpers/AuthCheck';
import './LoginComponent.css';

class LoginComponent extends React.Component {

    constructor(props) {
        super(props);

        if (AuthCheck) {
            this.props.logout();
        }

        this.state = {
            email: '',
            password: '',
            submited: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handelCloseAlert = this.handelCloseAlert.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password } = this.state;
        if (email && password) {
            this.props.login(email, password);
        }
    }

    handelCloseAlert(e) {
        this.props.clearAlert();
    }

    render() {
        const { loggingIn, error } = this.props;
        const { email, password, submitted } = this.state;
        return (
            <div className="main main-login">
                <div className="container">
                    <center>
                        <div className="middle">
                            <div id="login">
                                <div className="d-block d-sm-none mb-5">
                                    <img src={'/lg_izy.svg'} height="62" alt="Logo Izy" />
                                    <div className="clearfix"></div>
                                </div>
                                <form name="form" onSubmit={this.handleSubmit}>
                                    <fieldset className="clearfix">
                                        {error &&
                                            <div className="alert alert-warning alert-warning-outline alert-dismissible fade show p-3" role="alert">
                                                {error}
                                            </div>}
                                        <p>
                                            <span className="fa fa-envelope"></span>
                                            <input type="text" name="email" value={email} placeholder="Email" onChange={this.handleChange} required />
                                        </p>
                                        <p>
                                            <span className="fa fa-lock"></span>
                                            <input type="password" name="password" value={password} placeholder="Password" onChange={this.handleChange} required />
                                        </p>
                                        <div>
                                            <span className="span-forgot">
                                                <a className="small-text" href="#">Forgot password?</a>
                                            </span>
                                            <span className="span-signin">
                                                <input type="submit" value="Sign In" />
                                            </span>
                                        </div>
                                    </fieldset>
                                    <div className="clearfix"></div>
                                </form>
                                <div className="clearfix"></div>

                            </div>
                            <div className="logo d-none d-sm-block">
                                <img src={'/lg_izy.svg'} height="120" alt="Logo Izy" />
                                <div className="clearfix"></div>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    const loggingIn = state.AuthReducer.loggingIn;
    const error = state.AuthReducer.error;
    return { loggingIn, error };
}

const mapDispatchToProps =  {
    login: AuthAction.login,
    logout: AuthAction.logout,
    clearAlert: AuthAction.clearAlert
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);