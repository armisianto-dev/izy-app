import React from 'react';
import { connect } from 'react-redux';
import StringHelpers from '../../../helpers/StringHelpers';
import './DetailMenuComponent.css';

class DetailMenuComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileImg: '/dummy-food.png'
        }
    }

    render() {
        const product = this.props.match.params.product;
        const { menus } = this.props;
        const item = menus.items.filter(m => StringHelpers.string_to_slug(m.name) == product)
        const menu = item[0];
        return (
            <div className="row mt-5">
                <div className="col-9 mx-auto">
                    <div className="card shadow">
                        <div className="card-body">
                            <div className="media">
                                <img className="align-self-start mr-3" src={this.state.fileImg} />
                                <div className="media-body">
                                    <span className="badge badge-pill badge-info text-uppercase">{menu.category.name}</span>
                                    <h5 className="mt-0 text-capitalize">{menu.name}</h5>
                                    <small>Available at {menu.requested_time}</small>
                                    <p>{menu.description}</p>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { MenuReducer } = state;
    const menus = MenuReducer
    return { menus };
}

export default connect(mapStateToProps, null)(DetailMenuComponent);