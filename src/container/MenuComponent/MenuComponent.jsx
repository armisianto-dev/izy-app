import React, { Fragment } from 'react';
import { Modal } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import GlassMagnifier from 'react-image-magnifiers/dist/GlassMagnifier';
import { connect } from 'react-redux';
import Select from 'react-select';
import SimpleReactValidator from 'simple-react-validator';
import MenuAction from '../../actions/MenuAction';
import CardMenuComponent from '../../component/CardMenuComponent/CardMenuComponent';
import DateTimeHelpers from '../../helpers/DateTimeHelpers';
import './MenuComponent.css';
const defaultInputs = {
    id: '',
    name: '',
    description: '',
    image_url: '/dummy-food.png',
    price: {
        value: 0,
        currency: "IDR"
    },
    category: {
        id: 1
    },
    requested_time: '0001-01-01T00:00:00Z',
    product_type_id: 1,
    out_of_stock: false,
    duration: {
        value: 0,
        format: ''
    }
}

class MenuComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formInputs: {
                id: '',
                name: '',
                description: '',
                image_url: '/dummy-food.png',
                price: {
                    value: 0,
                    currency: "IDR"
                },
                category: {
                    id: 1
                },
                requested_time: '0001-01-01T00:00:00Z',
                product_type_id: 1,
                out_of_stock: false,
                duration: {
                    value: 0,
                    format: ''
                }
            }
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleShowModalDetail = this.handleShowModalDetail.bind(this);
        this.handleShowModalAdd = this.handleShowModalAdd.bind(this);
        this.handleShowModalEdit = this.handleShowModalEdit.bind(this);
        this.handleSelectCategoryChange = this.handleSelectCategoryChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);

        // validator
        this.validator = new SimpleReactValidator({
            element: (message, className) => <span className={className}>{message}</span>,
            locale: 'id',
            messages: {
                required: ':attribute is required.',
                email: 'Email not valid.'
            }
        });
    }

    componentDidMount() {
        this.props.getMenus();
    }

    componentWillReceiveProps(nextProps) {
        const saved = nextProps.menu.saved;

        if (saved === true) {
            this.props.getMenus();
            const formInputs = defaultInputs;
            this.setState({
                ...this.state,
                formInputs
            });
        }
    }

    getAll() {
        this.props.getMenus();
    }

    handleChange(updatedInputs) {
        const formInputs = updatedInputs;
        this.setState({
            ...this.state,
            formInputs
        });
    }

    handleShowModalDetail(item) {
        this.props.getDetail(item, 'detail');
    }

    handleShowModalAdd() {
        this.props.getFormInputs({}, 'add');
    }

    handleShowModalEdit(item) {
        const formInputs = item;
        this.setState({
            ...this.state,
            formInputs
        }, () => {
            this.props.getFormInputs(item, 'edit');
        });

    }

    handleCloseModal() {
        this.props.closeModal();
        const formInputs = defaultInputs;
        this.setState({
            ...this.state,
            formInputs
        });
        this.validator.hideMessages();
    }

    handleSelectCategoryChange(selectedOption) {
        this.handleChange({
            ...this.state.formInputs,
            category: {
                id: selectedOption.value
            }
        });
    }

    handleTimeChange(time) {
        this.handleChange({
            ...this.state.formInputs,
            requested_time: DateTimeHelpers.formatTimeUTC(time, +7)
        });
    }

    handleImageChange(event) {
        this.handleChange({
            ...this.state.formInputs,
            image_url: URL.createObjectURL(event.target.files[0])
        });
    }

    submitProcess(formInputs) {
        if (formInputs.id === '') {
            this.props.insertMenu(formInputs);
        } else {
            this.props.updateMenu(formInputs);
        }

    }

    handleSubmitForm(e) {
        e.preventDefault();
        if (this.validator.allValid()) {
            this.submitProcess(this.state.formInputs);
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render() {
        const { menu } = this.props;
        const formInputs = this.state.formInputs ;

        return (<div className="container mt-5">
            <div className="row">
                {menu && menu.loading && <div className="col-12 text-center"><i className="fa fa-spin fa-spinner"></i> Loading</div>}
                {menu && menu.items &&
                    <div className="col-md-10 col-12 mx-auto pb-5">
                        <ul className="list-group shadow">
                        {menu.items.map((item, index) =>
                            <CardMenuComponent key={index} menu={item} handleShowModal={this.handleShowModalDetail} handleShowModalForm={this.handleShowModalEdit} />
                    )}
                        </ul>
                    </div>
                }
            </div>
            {menu && menu.item &&
                <ModalForms menu={menu} formInputs={formInputs} closeModal={this.handleCloseModal} submitForm={this.handleSubmitForm} selectCategoryChange={this.handleSelectCategoryChange} timeChange={this.handleTimeChange} imageChange={this.handleImageChange} inputChange={this.handleChange} validator={this.validator} />
            }
            <a className="floating-button" onClick={this.handleShowModalAdd}>
                <i className="fa fa-plus floating-button-icon"></i>
            </a>
        </div>)
    }
}

const DatePickerInput = (props) => {
    return (
        <DatePicker
            className="form-control"
            selected={props.time}
            onChange={date => props.timeChange(date)}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Requested Time"
            dateFormat="HH:mm"
            timeFormat="HH:mm"/>
    );
}

const ModalForms = (props) => {
    const { menu, formInputs, closeModal, submitForm, selectCategoryChange, timeChange, inputChange, validator } = props;

    const selectCategories = [];
    if (menu.categories) {
        menu.categories.map((category) => {
            // return selectCategories.push({ value: category.id, label: category.name });
            return selectCategories[category.id] = { value: category.id, label: category.name };
        })
    }
    return (<Modal show={menu.showModal} onHide={closeModal} size="lg" centered>
        {menu.form === 'detail' && menu.item &&
            <Fragment>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" className="text-capitalize">
                        {menu.item.name}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row px-4">
                        <div className="col-md-4 col-12 text-center p-0">
                            <GlassMagnifier
                                className="d-none d-sm-block p-2 border border-warning"
                                imageSrc="/dummy-food.png"
                                imageAlt={menu.item.name}
                                magnifierSize="50%"
                                square={true}
                            />
                            <GlassMagnifier
                                className="d-block d-sm-none p-2 border border-warning"
                                imageSrc="/dummy-food.png"
                                imageAlt={menu.item.name}
                                magnifierSize="75%"
                                square={true}
                            />
                        </div>
                        <div className="col-md-8 col-12 pt-3 pt-md-0">
                            <span className="badge badge-pill badge-info text-uppercase">{menu.item.category.name}</span>
                            <div className="clearfix"></div>
                            <h6 className="font-weight-bold my-2">{menu.item.price.currency} {menu.item.price.value.toLocaleString()}</h6>
                        <small><i className="fa fa-clock mr-1"></i>Available at {DateTimeHelpers.formatTimeID(menu.item.requested_time, +7)}</small>
                            <p>{menu.item.description}</p>
                        </div>
                    </div>
                </Modal.Body>
            </Fragment>
        }
        {(menu.form === 'add' || menu.form === 'edit') &&
            <Fragment>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" className="text-capitalize">
                    {menu.form === 'add' && 'Create New Item'}
                    {menu.form === 'edit' && 'Update Item'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form className="form-horizontal" onSubmit={submitForm}>
                        <div className="form-group">
                            <label className="col-12 control-label">Category</label>
                            <div className="col-md-4 col-sm-12 col-xs-12">
                                <Select
                                    defaultValue={selectCategories[formInputs.category.id]}
                                    options={selectCategories}
                                    isSearchable={true}
                                    onChange={selectCategoryChange}
                            />
                            {validator.message('category', formInputs.category.id, 'required')}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-12 control-label">Name</label>
                            <div className="col-12">
                                <input type="text" className="form-control" name="name" value={formInputs.name} onChange={e => inputChange({ ...formInputs, name: e.target.value })} />
                            {validator.message('Name', formInputs.name, 'required', {
                                className: 'text-danger text-capitalize'
                                })}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-12 control-label">Description</label>
                            <div className="col-12">
                            <textarea className="form-control" onChange={e => inputChange({ ...formInputs, description: e.target.value })} value={formInputs.description}>{formInputs.description}</textarea>
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-12 control-label">Price</label>
                            <div className="col-12">
                            <input type="text" className="form-control" value={formInputs.price.value} onChange={e => inputChange({ ...formInputs, price: { ...formInputs.price, value: parseFloat(e.target.value) } })} />
                            {validator.message('Price', formInputs.name, 'required', {
                                className: 'text-danger text-capitalize'
                            })}
                        </div>
                        </div>
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-6 col-sm-12 mb-3">
                                    <div className="row mx-0">
                                        <label className="col-12 control-label">Request Time</label>
                                        <div className="col-12">
                                            <DatePickerInput timeChange={timeChange} time={DateTimeHelpers.convertDateTime(formInputs.requested_time, +7)} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-12">
                                    <div className="row mx-0">
                                        <label className="col-12 control-label">Image item</label>
                                        <div className="col-md-6 col-sm-12">
                                            <div className="imgUp w-100">
                                                <img id="foto" alt="Foto" className="imagePreview" src={formInputs.image_url} />
                                                <label className="btn btn-white btn-block text-green no-border">
                                                    Choose Image<input type="file" name="files" className="uploadFile img"
                                                        accept=".png, .jpg"
                                                        onChange={(event) => props.imageChange(event)} />
                                                </label>
                                                <i className="fa fa-times-circle fa-2x del"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-12 text-right">
                                <button type="submit" className="btn btn-md btn-success">
                                    {menu.saving && <span><i className="fa fa-spin fa-spinner mr-2"></i> Menyimpan</span>}
                                    {!menu.saving && <span>Simpan</span>}
                                </button>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
            </Fragment>
        }
    </Modal>);
}

function mapStateToProps(state) {
    const { MenuReducer } = state;
    const menu = MenuReducer;
    return { menu };
}

const mapDispatchToProps = {
    getMenus: MenuAction.getAll,
    getDetail: MenuAction.getDetail,
    getFormInputs: MenuAction.getDetail,
    closeModal: MenuAction.closeDetail,
    insertMenu: MenuAction.insertMenu,
    updateMenu: MenuAction.updateMenu
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuComponent);