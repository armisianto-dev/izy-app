import ArrayHelpers from '../helpers/ArrayHelpers';
import AxiosHeader from '../helpers/AxiosHeader';
import API from '../utils/axios-api';

const getAll = () => {
    const result = API.get('/product?product_type_id=1', AxiosHeader)
        .then(response => {
            let data = response.data;
            if (response.status === 200) {
                const categories = getCategories(data);
                const menus = { categories: categories, items: data };
                return menus;
            } else {
                const error = data.message;
                return Promise.reject(error);
            }
        });
    return result;
}

const getCategories = (items) => {
    const categories = [];
    items.map((item) => {
        return categories.push(item.category);
    });

    return ArrayHelpers.getUnique(categories, 'id');
}


const insertMenu = (params) => {
    const result = API.post('/product', JSON.stringify(params), AxiosHeader).then((response) => {
        let data = response.data;
        if (response.status === 200) {
            return data.result;
        } else {
            const error = data.message;
            return Promise.reject(error);
        }
    });
    return result;
}


const updateMenu = (params) => {
    const result = API.put('/product/' + params.id, JSON.stringify(params), AxiosHeader).then((response) => {
        let data = response.data;
        if (response.status === 200) {
            return data.result;
        } else {
            const error = data.message;
            return Promise.reject(error);
        }
    });
    return result;
}

const MenuService = {
    getAll,
    getCategories,
    insertMenu,
    updateMenu
}

export default MenuService;