import md5 from 'md5';
import AxiosHeader from '../helpers/AxiosHeader';
import API from '../utils/axios-api';

const login = (email, password) => {
    const postData = {
        'email': email,
        'password': md5(password)
    }

    const result = API.post('/login', JSON.stringify(postData), AxiosHeader).then((response) => {
        let data = response.data;
        let result = data.result;
        if (response.status === 200) {
            localStorage.setItem('user', JSON.stringify(result));
            return data.result;
        } else {
            const error = data.message;
            return Promise.reject(error);
        }
    });
    return result;
}

const logout = () => {
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 404) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }

            const error = data.message;
            console.log(error);
            return Promise.reject(error);
        }

        return data;
    });
}

const AuthService = {
    login,
    logout
}

export default AuthService;