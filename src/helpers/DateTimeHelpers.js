import StringHelpers from "./StringHelpers";

function calculateDateTime(offset) {
    // get current local time in milliseconds
    var date = new Date();
    var localTime = date.getTime();


    // get local timezone offset and convert to milliseconds
    var localOffset = date.getTimezoneOffset() * 60000;

    // obtain the UTC time in milliseconds
    var utc = localTime + localOffset;


    var newDateTime = utc + (3600000 * offset);

    var convertedDateTime = new Date(newDateTime);
    return convertedDateTime.toLocaleString();
}


function convertDateTime(datetime, offset) {
    // get current local time in milliseconds
    var date = new Date(datetime);
    var localTime = date.getTime();


    // get local timezone offset and convert to milliseconds
    var localOffset = date.getTimezoneOffset() * 60000;

    // obtain the UTC time in milliseconds
    var utc = localTime + localOffset;


    var newDateTime = utc + (3600000 * offset);

    var convertedDateTime = new Date(newDateTime);
    return convertedDateTime;
}

function convertDateTimeString(datetime, offset) {
    // get current local time in milliseconds
    var date = new Date(datetime);
    var localTime = date.getTime();


    // get local timezone offset and convert to milliseconds
    var localOffset = date.getTimezoneOffset() * 60000;

    // obtain the UTC time in milliseconds
    var utc = localTime + localOffset;


    var newDateTime = utc + (3600000 * offset);

    var convertedDateTime = new Date(newDateTime);
    return convertedDateTime.toLocaleString();
}

function convertDateTimeID(datetime, offset) {
    // get current local time in milliseconds
    var date = new Date(datetime);
    var localTime = date.getTime();


    // get local timezone offset and convert to milliseconds
    var localOffset = date.getTimezoneOffset() * 60000;

    // obtain the UTC time in milliseconds
    var utc = localTime + localOffset;


    var newDateTime = utc + (3600000 * offset);

    var convertedDateTime = new Date(newDateTime);

    var years = StringHelpers.padLeft(convertedDateTime.getFullYear(), 4, '0');
    var month = ("0" + convertedDateTime.getMonth()).slice(-2);
    var day = ("0" + convertedDateTime.getDay()).slice(-2);
    var hours = ("0" + convertedDateTime.getHours()).slice(-2);
    var minutes = ("0" + convertedDateTime.getMinutes()).slice(-2);

    return years + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':00';
}

function formatTimeID(datetime, offset) {
    // get current local time in milliseconds
    var date = new Date(datetime);
    var localTime = date.getTime();


    // get local timezone offset and convert to milliseconds
    var localOffset = date.getTimezoneOffset() * 60000;

    // obtain the UTC time in milliseconds
    var utc = localTime + localOffset;


    var newDateTime = utc + (3600000 * offset);

    var convertedDateTime = new Date(newDateTime);

    var hours = ("0" + convertedDateTime.getHours()).slice(-2);
    var minutes = ("0" + convertedDateTime.getMinutes()).slice(-2);
    var seconds = ("0" + convertedDateTime.getSeconds()).slice(-2);

    return hours + ':' + minutes + ':' + seconds;
}


function formatTimeUTC(datetime, offset) {
    // get current local time in milliseconds
    var date = new Date(datetime);
    const getDateString = (date.toISOString()).substr(0, 10);
    var localTime = date.getTime();


    // get local timezone offset and convert to milliseconds
    var localOffset = offset * 60 * 60000;

    // obtain the UTC time in milliseconds
    var utc = localTime - localOffset;

    var convertedDateTime = new Date(utc);

    var hours = ("0" + convertedDateTime.getHours()).slice(-2);
    var minutes = ("0" + convertedDateTime.getMinutes()).slice(-2);
    var seconds = ("0" + convertedDateTime.getSeconds()).slice(-2);

    return getDateString + 'T' + hours + ':' + minutes + ':' + seconds + 'Z';
}

const DateTimeHelpers = {
    calculateDateTime,
    convertDateTime,
    convertDateTimeString,
    convertDateTimeID,
    formatTimeID,
    formatTimeUTC
}

export default DateTimeHelpers;