
let user = JSON.parse(localStorage.getItem('user'));
let token = (user) ? user.token : '';
let basicHeader = {
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }
}

let authHeader = {
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Access-Control-Allow-Origin': '*',
        'X-App-Source': 'web'
    }
}

const AxiosHeader = (user && user.token) ? authHeader : basicHeader;

export default AxiosHeader;