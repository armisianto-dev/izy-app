import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import OperatorBaseComponent from './base/OperatorBaseComponent/OperatorBaseComponent';
import LoginComponent from './container/LoginComponent/LoginComponent';
import History from './helpers/History';

class App extends React.Component {

  render() {
    return (
      <BrowserRouter history={History}>
        <Switch>
          <Route path="/login" component={LoginComponent} />
          <OperatorBaseComponent />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
