import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const TopBarComponent = (props) => {
    return (
        <header>
            <nav className="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar bg-dark">
                <Link className="navbar-brand mr-0 mr-md-2" to="/">
                    <img src={process.env.PUBLIC_URL + '/lg_izy.svg'} alt="logo izy" height="48"/>
                </Link>
                <div className="navbar-nav-scroll">
                    <ul className="navbar-nav bd-navbar-nav flex-row">
                        <MenuLink activeOnlyWhenExact={true} to="/" label="Dashboard" />
                        <MenuLink to="/menu" label="Menu" />
                    </ul>
                </div>
                <ul className="navbar-nav ml-md-auto">
                    <li className="nav-item dropdown">
                        <a className="nav-item nav-link dropdown-toggle mr-md-2" href="#" id="bd-account" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {props.user.prefix} {props.user.given_name}
                        </a>
                        <div className="dropdown-menu dropdown-menu-md-right" aria-labelledby="bd-account">
                            <a className="dropdown-item" href="#">My Account</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#" onClick={props.actionLogout}>Sign Out</a>
                        </div>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

const MenuLink = ({ label, to, activeOnlyWhenExact }) => {
    let match = useRouteMatch({
        path: to,
        exact: activeOnlyWhenExact
    });

    return (
        <li className={match ? "nav-item active" : "nav-item"}>
            <Link className="nav-link" to={to}>{label}</Link>
        </li>
    );
}
export default TopBarComponent;