import React from 'react';
import { connect } from 'react-redux';
import MenuAction from '../../actions/MenuAction';
import StringHelpers from '../../helpers/StringHelpers';
import './CardMenuComponent.css';

class CardMenuComponent extends React.Component {
    constructor(props) {
        super(props);

    }

    handleShowModal = (ev) => {
        ev.preventDefault();
        this.props.handleShowModal(this.props.menu);
    }


    handleShowModalForm = (ev) => {
        ev.preventDefault();
        this.props.handleShowModalForm(this.props.menu);
    }

    render() {
        const menu = this.props.menu;
        const item = menu;
        const slug = StringHelpers.string_to_slug(item.name);

        return (
            <li className="list-group-item">
                <div className="media align-items-lg-center flex-column flex-lg-row p-3">
                    <div className="media-body order-2 order-lg-1">
                        <span className="badge badge-pill badge-info text-uppercase">{item.category.name}</span>
                        <h5 className="mt-0 font-weight-bold text-capitalize mb-2">{item.name}</h5>
                        <p className="font-italic text-muted mb-0 small">{item.description}</p>
                        <div className="d-flex align-items-center justify-content-between mt-1">
                            <h6 className="font-weight-bold my-2">{item.price.currency} {item.price.value.toLocaleString()}</h6>
                        </div>
                    </div>
                    <img src={'/dummy-food.png'} alt={item.name} width="200" className="ml-lg-5 order-1 order-lg-2" />
                    <div className="media-button-action">
                        <a onClick={this.handleShowModal} className="btn btn-md btn-outline-primary mr-4">
                            Detail
                        </a>
                        <a onClick={this.handleShowModalForm} className="btn btn-md btn-outline-warning">
                            Edit
                        </a>
                    </div>
                </div>
            </li>
        )
    }
}

const mapDispatchToProps = {
    getDetail: MenuAction.getDetail
}

export default connect(null, mapDispatchToProps)(CardMenuComponent);